# alpine-torrssen2
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-torrssen2)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-torrssen2)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-torrssen2/x64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64
* Appplication : [torrssen2](https://github.com/tarpha/torrssen2)
    - Torrent RSS Site를 등록/관리하고, 다운로드를 요청/관리하고, 자동 다운로드를 수행하는 Webapp (Spring Boot + Nuxt.js)



----------------------------------------
#### Run

```sh
docker run -d \
           -p 8080:8080/tcp \
           -v /conf.d:/conf.d \
           forumi0721/alpine-torrssen2:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:8080/](http://localhost:8080/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8080/tcp           | torrssen2 port                                   |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /conf.d            | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | Application execute username (default : forumi721) |
| RUN_USER_UID       | Application execute user uid (default : 1000)    |
| RUN_USER_GID       | Application execute user gid (default : 100)     |

