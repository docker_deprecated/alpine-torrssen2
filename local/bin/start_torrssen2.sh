#!/bin/sh

mkdir -p /conf.d/torrssen2

chown -R ${RUN_USER_UID:-1000}:${RUN_USER_GID:-100} /conf.d/torrssen2

cd /conf.d/torrssen2

exec su-exec ${RUN_USER_NAME:-forumi0721} java -jar /app/torrssen2/torrssen2-*.jar

